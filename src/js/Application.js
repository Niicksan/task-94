import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas() {
    let asa = this.emojis.map(emoji => {
      return emoji + this.banana;
    });

    asa.forEach(emoji => {
      const div = document.querySelector("#emojis");

      let p = document.createElement("p");
      p.textContent = emoji;
      div.appendChild(p);
    });
  }
}